import React, {useState, useEffect} from 'react';
import { Button, FloatingLabel, Form } from 'react-bootstrap'

import AirportInfo from './AirportInfo';
import Footer from './Footer';
import Header from './Header';


import { airportInfo } from '../functions/functions';

const Start = () => {
    const[airport, setAirport] = useState("");
    const [message, setMessage] = useState(null);

    const handleChange = event => {
        setMessage(event.target.value);

    };
    function clickOnSearchBtn(){
        airportInfo(setAirport, message);

    }
    useEffect(() => {
        airportInfo(setAirport)
    
      }, [])
    return (
        <>  
            <Header />
            <FloatingLabel
                controlId="floatingInput"
                label="Airport Code (iata)"
                className="mb-3 formplace form-control__search">
                <Form.Control 
                    type="text" 
                    placeholder='EZE'
                    onChange={handleChange}
                />

                <Button 
                    className="search"
                    variant="primary" 
                    as="input" 
                    type="submit" 
                    value="Search"
                    onClick={clickOnSearchBtn}
                />

            </FloatingLabel>
            <AirportInfo airportApi={airport}/>
            <Footer />
        </>

    )

}

export default Start