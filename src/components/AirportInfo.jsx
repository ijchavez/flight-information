import React from 'react'

import '../Styles/airportinfo.css';

const AirportInfo = ({ airportApi } ) => {
  return (
    <>
        {airportApi !== null && !airportApi.error ? (
            <div className="airport__container">
                <div className="airport__content">
                    <div className="airport__details">
                        <div className="airport__iata">
                            <h5>ID: {airportApi.id}</h5>
                            <h5>iata: {airportApi.iata}</h5>
                            <h5>icao: {airportApi.icao}</h5>
                            <h5>Name: {airportApi.name}</h5>
                            <h5>Location: {airportApi.location}</h5>
                        </div>

                    </div>
                </div>
                <div className="airport__content">
                    <div className="airport__details">
                        <h5>Address:</h5>
                        <div className="airport__address">
                            <h5>{airportApi.street_number}, {airportApi.street}</h5>
                            <h5>{airportApi.postal_code}, {airportApi.location} </h5>
                            <h5>{airportApi.city}, {airportApi.county}, {airportApi.state}</h5>

                        </div>
                        <div>
                            <h5>Country: {airportApi.country_iso}, {airportApi.country}</h5>

                        </div>
                    </div>

                </div> 
                <div className="airport__content">
                    <div className="airport__details">
                        <h5>Phone: {airportApi.phone}</h5>
                        <h5>GPS Coordenates: {airportApi.latitude}, {airportApi.longitude}</h5>
                        <h5>ID: {airportApi.uct}</h5>
                        <div className ="airport__website">
                            <h5 className="title__website">Website:</h5>
                            <span >
                                <a href={airportApi.website}> {airportApi.website}</a>
                            
                            </span>
                                
                        
                        </div>

                    </div>
                
                </div>

            </div>

        ) : (<div className="airport__container">
                <div className="airport__content">
                    <div className="airport__details">
                        <div className="airport__iata">
                            <h2>Message:</h2>
                            <h5>{airportApi.error.text}</h5>

                        </div>
            
                    </div>
            
                </div>
    
             </div>
            )}

    </>

  )

}

export default AirportInfo