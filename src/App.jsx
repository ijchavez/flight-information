import React from "react";
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import Start from './components/Start';

import "bootstrap/dist/css/bootstrap.min.css";

import './Styles/app.css';

function App() {
  return (
    <div className="container">
      <BrowserRouter>
         <Routes>
           <Route path="/" element = {<Start/>}></Route>
           
         </Routes>
        
      </BrowserRouter>

    </div>
  );
}

export default App;
