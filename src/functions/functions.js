import axios from 'axios';

const airportURL = 'https://airport-info.p.rapidapi.com/airport';

let host = 'X-RapidAPI-Host';
let key = 'X-RapidAPI-Key';

let headersMap = new Map([
    [host, 'airport-info.p.rapidapi.com'],
    [key, '5fb88ca518msh8137df7e4c1aed0p11dfaajsnc4d52bac0835']

]);

function configuration(iata){
    let config = {
        headers: { 
          'X-RapidAPI-Host': headersMap.get(host), 
          'X-RapidAPI-Key': headersMap.get(key)

        },
        params: {
          'iata': iata
        
        }

    }
    return config;

}
const airportInfo = async (state, iata) => {
    const petition = await axios.get(airportURL, configuration(iata));
    state(petition.data);

}
export{
    airportInfo,
    configuration

}